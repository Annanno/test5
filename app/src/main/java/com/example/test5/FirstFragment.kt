package com.example.test5

import android.os.Bundle
import android.os.SharedMemory
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.test5.databinding.FragmentFirstBinding
import kotlin.time.TestTimeSource


class FirstFragment : Fragment() {

    private var _binding: FragmentFirstBinding? = null
    private val binding get() = _binding!!
    private val viewModel: TestViewModel by viewModels()
    private lateinit var parentAdapter: ParentAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentFirstBinding.inflate(inflater, container, false)
        return binding.root


    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        /// listeners


        binding.parentRecyclerView.layoutManager = LinearLayoutManager(
            context,
            LinearLayoutManager.VERTICAL, false
        )
        parentAdapter = ParentAdapter()
        binding.parentRecyclerView.adapter = parentAdapter



        viewModel.parsedJson.observe(viewLifecycleOwner, {
            //update ui
        })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }


}