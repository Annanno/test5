package com.example.test5

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class ChildAdapter(var memberData: MutableList<Model.ModelSubList>) :
    RecyclerView.Adapter<ChildAdapter.DataViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = DataViewHolder(
        LayoutInflater.from(parent.context).inflate(
            R.layout.item_layout, parent,
            false
        )
    )

    override fun getItemCount() = memberData.size


    override fun onBindViewHolder(holder: DataViewHolder, position: Int) {
    }

    inner class DataViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)



}