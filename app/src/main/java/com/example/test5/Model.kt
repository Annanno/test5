package com.example.test5

data class Model(var model: MutableList<ModelSubList>) {

    data class ModelSubList(var modelSubList: MutableList<ModelSubListItem>)

    data class ModelSubListItem(
        val field_id: Int,
        val field_type: String,
        val hint: String,
        val icon: String,
        val is_active: Boolean,
        val keyboard: String,
        val required: String
    )
}

