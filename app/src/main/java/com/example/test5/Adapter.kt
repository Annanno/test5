package com.example.test5

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.NonNull
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.test5.databinding.FragmentFirstBinding
import com.example.test5.databinding.ParentItemLayoutBinding

class ParentAdapter :
    RecyclerView.Adapter<ParentAdapter.DataViewHolder>() {

    var modelData: MutableList<Model> = mutableListOf()


    //    var callback: ClickItem? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = DataViewHolder(
        ParentItemLayoutBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
    )

    override fun onBindViewHolder(holder: ParentAdapter.DataViewHolder, position: Int) {
        holder.onBind(modelData[position])

    }


    override fun getItemCount() = modelData.size

    inner class DataViewHolder(private val binding: ParentItemLayoutBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun onBind(result: Model) {
            val childMembersAdapter = ChildAdapter(result.model)
            binding.childRecyclerView.layoutManager =
                LinearLayoutManager(binding.root.context, LinearLayoutManager.VERTICAL, false)
            binding.childRecyclerView.adapter = childMembersAdapter

        }


    }


}